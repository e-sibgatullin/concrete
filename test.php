<?php
namespace Concrete;
require_once __DIR__."/vendor/autoload.php";

use Modbus\ModbusTCP;
use Modbus\ModbusParser;
use Modbus\PacketBuilder;

$params = [
    'port' => [
        'openPort' => 1,
        'mPort' => 1,
        'maxMass' => 50
        ], 
    'connect' => [
        'host' => '127.0.1.1', 
        'port' => '502', 
        'devAddr' => 1
        ]
    ];

$hopper = DeviceFabricate::build($params);

try {
    $hopper->open();
    var_dump($hopper->curentState());
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}






