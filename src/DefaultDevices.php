<?php
namespace Concrete;

class DefaultDevices 
{
    CONST PORT_CLOSE = 0;
    CONST PORT_OPEN = 1;
    
    protected $modbus;    
    protected $openPort; 
    protected $portState = self::PORT_CLOSE;

    public function __construct(\Modbus\ModbusTCP $modbus, array $port = ['openPort'=>1]) 
    {
        $this->modbus = $modbus;
        $this->openPort = $port['openPort'];          
    }
    
    public function open() 
    {
        if($this->portState){
            return;
        }
        
        $this->modbus                
                ->fc($this->modbus::FC5)
                ->startReg($this->openPort)
                ->valReg(self::PORT_OPEN);
        $this->modbus->send();
        $this->portState = self::PORT_OPEN;
    }
    
    public function close() 
    {
        if(!$this->portState){
            return;
        }
        
        $this->modbus                
                ->fc($this->modbus::FC5)
                ->startReg($this->openPort)
                ->valReg(self::PORT_CLOSE);
        $this->modbus->send();
        $this->portState = self::PORT_CLOSE;
    }
    
    public function curentState() 
    {
        return ['openPort' => $this->portState];
    }
}
