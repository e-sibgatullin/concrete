<?php
namespace Concrete;

class Dispenser extends DefaultDevices
{    
    protected $mPort;
    protected $maxMass;    
    protected $factMass;
    protected $currMass;
    protected $limitMass;

    public function __construct(\Modbus\ModbusTCP $modbus, array $port = ['openPort'=>1, 'mPort'=>1, 'maxMass' => 50]) 
    {
        parent::__construct($modbus, $port);        
        $this->mPort = $port['mPort'];        
        $this->maxMass = $port['maxMass'];
        $this->limitMass = $this->maxMass;        
    }   
            
    public function isEnough() 
    {        
        $limitMass = min($this->maxMass, $this->limitMass);
        if($this->getCurrentMass() >= $limitMass){            
            return true;
        }
        
        return false;
    }
    
    public function setLimitMass($mass) 
    {
        if(empty($mass)){
            return;
        }
        
        $this->limitMass = $mass;
    }
    
    public function isEmpty($update = false)
    {        
        $mass = $update ? $this->getCurrentMass() : $this->currMass;
        $emptyDisp = $mass <= 0 ? true : false;
        return $emptyDisp;
    }
    
    public function getFactMass() 
    {
        return $this->factMass ?: 0;
    }
    
    private function getCurrentMass() 
    {
        $this->modbus                
                ->fc($this->modbus::FC3)
                ->dataTYpe([\Modbus\PacketBuilder::DOUBLE])
                ->startReg($this->port['mPort']);
        $responseData = $this->modbus->send()->getResponse();
        $mass = array_shift($responseData);
        $this->currMass = $mass <= 0 ? 0 : $mass;
        return $this->currMass;
    }  
    
    public function curentState() 
    {
        return [
            parent::curentState(), 
            'currentMass' => $this->getCurrentMass(),
                ];
    }
}
