<?php
namespace Concrete;

class Concrete 
{
    CONST AUTO              = 1;
    CONST MANUAL            = 0;
    CONST MSG_STATE_INFO    = 'info';
    CONST MSG_STATE_ERROR   = 'error';
    CONST DEFAULT_QUANTITY  = 1; 
    CONST STAGE_FREE        = 0;
    CONST STAGE_INIT        = 1;
    CONST STAGE_DOSING      = 2;
    CONST STAGE_MIXING      = 3;
    
    protected $recipe;     
    protected $state;
    protected $wsConnection;
    protected $currentRecipeWork;
    protected $storageDev = [];    
    protected $responseData = [];    
    protected $work = self::STAGE_FREE;
    protected $quantityProcces;
    protected $storageDevWork;

    public function __construct($recipe) 
    {
        $this->recipe = new \SplDoublyLinkedList();       
    }
    
    public function addQueue($recipe, $quantity = self::DEFAULT_QUANTITY) 
    {
        $msg        = "Рецепт $recipe добавлен в очередь с количеством кубов $quantity";
        $data       = [$recipe => $quantity];
        $this->recipe->push($data);        
        $this->sendMsg($msg);
    }
    
    public function make($mode = self::AUTO, $recipe = null, $quantity = null) 
    {
        if(!empty($recipe)){
            $this->addQueue($recipe, $quantity);
        }
        
        if($this->recipe->isEmpty()){
            $this->work = self::STAGE_FREE;
            $this->sendMsg("Нет рецептов для изготовления бетона.", self::MSG_STATE_ERROR);            
            return;
        }  
        
        $this->work = self::STAGE_INIT;
    }   
    
    private function sendMsg($msg, string $state = self::MSG_STATE_INFO) 
    {
        $msg = json_encode([            
            'state' => $state, 
            'work' => $this->work,
            'data' => $msg]);
        
        $this->wsConnection->send($msg);                
    }   
 
}
