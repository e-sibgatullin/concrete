<?php
namespace Concrete;

class Mixer extends DefaultDevices
{
    protected $startPort;
    protected $iPort;
    protected $stateStartPort = self::PORT_CLOSE;

    public function __construct(\Modbus\ModbusTCP $modbus, 
            array $port = ['openPort'=>1, 'startPort'=>1, 'IPort'=>1]) 
    {
        parent::__construct($modbus, $port);
        
        $this->startPort = $port['startPort'];
        $this->iPort = $port['iPort'];
    }
            
    public function start() 
    {
        if($this->stateStartPort){
            return;
        }
        
        $this->modbus                
                ->fc($this->modbus::FC5)
                ->startReg($this->startPort)
                ->valReg(self::PORT_OPEN);
        
        $this->modbus->send();
        $this->stateStartPort = self::PORT_OPEN;
    }
    
    public function stop() 
    {
        $this->modbus                
                ->fc($this->modbus::FC5)
                ->startReg($this->startPort)
                ->valReg(self::PORT_CLOSE);
        
        $this->modbus->send();
        $this->stateStartPort = self::PORT_CLOSE;
    }
    
    public function curentState() 
    {
        return [
            parent::curentState(),
            'startPort' => $this->stateStartPort,
            'iPort' => $this->curentAmper()
        ];
    }
    
    public function curentAmper() 
    {
        $this->modbus                
                ->fc($this->modbus::FC3)
                ->dataTYpe([\Modbus\PacketBuilder::DOUBLE])
                ->startReg($this->port['IPort']);
        $this->modbus->send()->getResponse();
    }
}
