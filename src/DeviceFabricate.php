<?php
namespace Concrete;

class DeviceFabricate 
{
    static $recon = false;
    private static $modbus;    
    
    static public function build(array $params, string $devType = 'DefaultDevices') 
    {        
        $connection = self::initModbus($params['connect']);        
        $devClass = "\\Concrete\\".$devType;
        return new $devClass($connection, $params['port']);
    }
    
    private static function initModbus($params) 
    {
        if(empty(self::$modbus) || self::$recon){
            self::modbusReconn($params);
        }
        
        return self::$modbus;
    }
    
    private static function modbusReconn($params) 
    {
        self::$modbus = new \Modbus\ModbusTCP(
            ["host"=>$params['host'], "port"=>$params['port'], "devAddr"=>$params['devAddr']],         
            new \Modbus\ModbusParser(),
            new \Modbus\PacketBuilder());
    }
}
